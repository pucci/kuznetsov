#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>

#define tamanho 3 //ordem da matriz


struct lines
{
  //sum = quantidade de 1s em cada linha
  int sum;
  int rows[tamanho];
};

//"comparator", usa o sum de cada elemento do conjunto pra ordenar de forma decrescente
bool compare(const lines &a, const lines &b)
{
  return a.sum < b.sum;
}


double kuznetsov(lines m[]){

    double perm = -1;
    double estimador = 0;

    if(m[0].sum == 0){ //step 1, se b1 = 0, per A = 0.
        return perm = 0;
    }

    int realizations = 1000;
    for(int N=0; N<realizations; N++){
        int interacoes = 6; //???? quantas vezes isso faz
        for(int k = 0; k < interacoes; k++){


            int aux[tamanho]; // em cada intera��o controla de qual coluna ele ja pegou um valor pra ela n�o ser mais usada
            for(int a= 0; a< tamanho; a++){
                aux[a] = 1;
            }


            int j; //coluna a ser selecionada da matriz
            int mult = 1; //valor da multiplica��oo dos elementos selecionados

            for(int i = 0; i < tamanho; i++){
                j = rand() % tamanho;
                while(aux[j] == 0)
                    j = rand() % tamanho;

                aux[j] = 0;
                if(m[i].rows[j]==0){ //se cair em algum elemento 0
                    mult = 0;
                    break;
                }
            }
            if(mult == 1) estimador+=1; //multiplica��o dos elementos da linha deu 1, soma ao estimador e volta para intera��es
            if(mult == 0) mult = 1; //se achou um zero, n�o connsidera no estimador, zera mult e continua
        }
    }
    perm = estimador/realizations; //m�dia de n estimadores
    return perm;
}

int main(){

    srand(time(NULL)); //se n�o fizer isso rand usa mesmo seed

    //inicia matriz
    lines M[tamanho];
    M[0]= {3,{1,1,1}};
    M[1]= {2,{0,1,1}};
    M[2]= {1,{1,0,0}};

    //sort na matriz, coloca maior numero de 0s em cima
    std::sort(&M[0], &M[tamanho], compare);

    //print matriz
    for (int i=0;i<tamanho;i++) {
        for (int j=0;j<tamanho;j++)
            printf("%d",M[i].rows[j]);
            printf("\n");
    }
    printf("\n");

    printf("%f",kuznetsov(M));

}
